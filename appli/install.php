<?php

include("config.php");
include("db.php");

$drop_user_table = "DROP TABLE IF EXISTS users";

$create_user_table = "
CREATE TABLE `users` (
`login` VARCHAR( 16 ) NOT NULL ,
`passwd` VARCHAR( 64 ) NOT NULL ,
`cookie` VARCHAR( 32 ),
`isadmin` BOOL NOT NULL DEFAULT FALSE,
`lastlogin` BIGINT UNSIGNED NULL DEFAULT NULL,
PRIMARY KEY ( `login` )
) ";

$db->query($drop_user_table);
$db->query($create_user_table);

createUser("user1", "8b7008729975d0398bdcdae4c720edc1462edc77") or die("Impossible de cr�er l'utilisateur 'user1'");
createUser("user2", "d6ddf672450e30e4bf3861466bb12458001df596", 1) or die("Impossible de cr�er l'utilisateur 'user2'");
createUser("evan", "178b62e73cc3fab9e30bf27b2372c9ff92401915") or die("Impossible de cr�er l'utilisateur 'evan'");


?>
