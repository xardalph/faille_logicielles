<?php

include_once("config.php");

include_once("db.php");

// Verify user cnx
$user = null;
include("user.php");
recognizeUser();

// File currently shown
$currentFile = null;

// Verify action var
$action=$_POST["action"];
if(!isset($action)) {

    $action = $_GET["action"];
    if (!isset($action)) {
        $action = "home";
    }
}
while($action)
{
    $file = "actions/".$action.".php";
    $action = "";
    include($file);
}

?>
