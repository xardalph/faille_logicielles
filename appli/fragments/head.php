<html>
<head><title>My Virtual Folder</title></head>
<style>

body {
    background-color: #d0d0d0;
    margin: 0;
}

h1 {
    background-color: #ffffff;
    padding: 16px;
    font-style: italic;
    border-bottom: black 1px solid;
}

#body {
    margin: 16px;
}

#connect, #admin {
    float: left;
    clear: left;
    padding: 16px;
    width: 360px;
    background-color: #ffffff;
    border: black 1px solid;
}

#filelist, #fileview {
    margin-left: 416px;
    background-color: #ffffff;
    border: black 1px solid;
}

#fileview #path {
    padding: 8px;
    border-bottom: grey 1px dotted;
}


#fileview #content {
    padding: 8px;
    border-left: grey 8px solid;
}


</style>
<body>
<h1><a href=".">My virtual folder</a></h1>

<div id="body">

<?php

if(isset($msg))
{
    foreach ($msg as $message)
        echo "<p>$message</p>\n";
    echo "<hr />\n";
}
?>
