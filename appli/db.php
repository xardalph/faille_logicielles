<?php

// Intend to connect to database and select the wanted db:
//$conf["db"]["cnx"] = mysql_connect($conf["db"]["server"], $conf["db"]["user"], $conf["db"]["passwd"])
$db = new mysqli($conf["db"]["server"], $conf["db"]["user"], $conf["db"]["passwd"], $conf["db"]["schema"]);
if($db->connect_errno)
{
    die("Impossible de se connecter à la base de données: " . $db->connect_errno . " - " . $db->connect_error);
}


// Declare some usefull functions:

function getNameFromCookie($cookie){
    global $conf, $db;
    $query = "SELECT `login` FROM `users` WHERE `cookie`='$cookie'";
    $res = $db->query($query);
    if($res->num_rows==0)
        return "";
    $row = $res->fetch_assoc();
    return $row["login"];
}

function validateLoginPasswd($login, $passwd){
    global $conf, $db;
    $hashpasswd = sha1($passwd);

    $query = $db->prepare("SELECT login FROM users WHERE login = ? AND passwd = ?");
    $query->bind_param('ss',$login ,$hashpasswd);
    $query->execute();
    $query->bind_result();
    $res = $query->fetch();

    return $res;
}

function setUserCookie($login, $cookie){
    global $conf, $db;

    $query = $db->prepare("UPDATE users SET cookie=? WHERE login=? ");
    $query->bind_param('ss', $cookie, $login);
    $query->execute();


}

function hasUser($login){
    global $conf, $db;

    $query = $db->prepare("SELECT login FROM users WHERE login = ? ");
    $query->bind_param('s', $login);
    $query->execute();
    $query->store_result();

    return $query->num_rows>0;
}

function createUser($login, $passwd, $isadmin=0){
    global $conf, $db;

    $query = $db->prepare("INSERT INTO users (login ,passwd ,isadmin) VALUES (?, ?, ?)");
    $query->bind_param('ssi', $login, $passwd, $isadmin);
    $query->execute();
    return $query->bind_result();



}

function userIsAdmin($login){
    global $conf, $db;

    $query = $db->prepare("SELECT * FROM users WHERE login = ? ");
    $query->bind_param('s', $login);
    $query->execute();

    $res = $query->get_result();

    if ($res->num_rows === 0){

        return $query->num_rows;
    }

    while($row = $res->fetch_row()){

        return $row[3];
    }

}

function getUserList($filter){
    global $conf, $db;
    $filter = stripslashes($filter);
    $query = "SELECT login FROM users";
    if($filter)
        $query .= " WHERE login LIKE '%$filter%'";
    $res = $db->query($query, $conf["db"]["cnx"]);
    if($res)
    {
        while($row = $res->fetch_assoc())
        {
            $users[] = $row["login"];
        }
    }
    return $users;
}

function removeUserEntry($login){
    global $conf, $db;
    $query = "DELETE FROM `users` WHERE `login`='$login'";
    $db->query($query);
}

function addtimestamp($login){
    global $conf, $db;

    $query = $db->prepare("UPDATE users SET lastlogin = ? WHERE login = ?");
    $time = time();
    #initial query
    #$query ="UPDATE `users` SET `lastlogin`='$time' WHERE `login` = '$login'";

    $query->bind_param('ss', $time, $login);
    $query->execute();

    return ;
}

function lastfaillogin($login){
    global $conf, $db;
    $query = "SELECT lastlogin FROM `users` WHERE `login`='$login'";
    $res = $db->query($query);
    #echo "fonction lastfaillogin  ".$res->num_rows;
    if ($res->num_rows==0) {
        return False;
    }
    elseif($res->num_rows == 1) {
        #echo "<br/> elseif =1 <br/>";
        $row = $res->fetch_assoc();
        $lastlogin = $row['lastlogin'] + 15;
        #echo gettype($lastlogin) . $lastlogin;

        if ($lastlogin >= time()) {
            #trop tot on jette
            #echo "trop tot on jette";
            return True;
        } else {
            return False;
        }
    }
}

function deletecookiebd($login){

        global $conf, $db;
        error_log($login, 0);

        $query = $db->prepare("UPDATE users SET cookie='' WHERE login= ?");
        $query->bind_param('s', $login);
        error_log($query->sqlstate, 0);
        $query->execute();
        return;

}

?>
