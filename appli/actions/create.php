<?php
session_start();

include_once $_SERVER['DOCUMENT_ROOT'] . '/tp1/securimage/securimage.php';

$securimage = new Securimage();


if(!$_POST["login"] || !$_POST["login"])
{
    $msg[]="Login and password must be precised";
}
else if(hasUser($_POST["login"]))
{
    $msg[]="Already existing login, please choose a different one.";
}
# test if captcha code was succesfully typed
elseif ($securimage->check($_POST['captcha_code']) == false) {
    # if not, print a message for that
    $msg[]="error in captcha code, please retry";
}

else
{
    if(createUser($_POST["login"], $_POST["passwd"]))
        $msg[]="Account correctly created, please connect.";
    else
        $msg[]="Error while creating account, please retry.";
}

$action = "home";

?>
