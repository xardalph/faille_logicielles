<?php

if($user)
{
    if($_FILES["sentfile"] && is_uploaded_file($_FILES["sentfile"]["tmp_name"]))
    {
        $dest = getUserRepositoryPath($user["login"])."/".basename($_FILES["sentfile"]["name"]);
        move_uploaded_file($_FILES["sentfile"]["tmp_name"], $dest);
        chmod($dest, 0777);
    }
}
else
{
    $msg[] = "You must be connected to upload files.";
}

$action = "home";

?>
