<?php

if($user)
{

    $currentFile = $_GET["file"];
    if($currentFile)
    {
        $path = getUserRepositoryPath($user["login"]);
        $filename = $path."/".$currentFile;
        if(unlink($filename))
        $msg[] = "$currentFile has been removed.";

        unset($currentFile);
        unset($filename);
    }

}
else
{
    $msg[] = "You must be connected to delete files.";
}

$action = "home";

?>
