<?php

function nameToType($name){
    $end = strtolower(strrchr($name,'.'));

    if($end==".txt")
        return "text";
    if($end==".jpeg" || $end==".jpg" || $end==".gif" || $end==".png")
        return "image";
    return "other";
}

if($user)
{
    $currentFile = $_GET["file"];
    $fullView    = isset($_GET["fullView"]);

    if($currentFile)
    {
        $path = getUserRepositoryPath($user["login"]);
        $filename = $path."/".$currentFile;

        $type = nameToType($filename);

        if($type=="other")
        {
            $action = "view";
        }
        else
        {
            $type = nameToType($filename);
            $action = "home";
        }
    }
}
else
{
    $msg[] = "You must be connected to show files.";
    $action = "home";
}

?>
