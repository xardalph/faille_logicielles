<?php

function nameToType($name){
    $end = strtolower(strrchr($name,'.'));

    if($end==".txt")
        return "text";
    if($end==".jpeg" || $end==".jpg" || $end==".gif" || $end==".png")
        return "image";
    return "other";
}

if($user)
{
    $currentFile = $_GET["file"];

    if($currentFile)
    {
        $path = getUserRepositoryPath($user["login"]);
        $filename = $path."/".$currentFile;
        $type = nameToType($path);

        if($file = fopen($filename, "rb"))
        {
            header("Content-Type: ".$type);
            header("Content-Length: ".filesize($filename));
            fpassthru($file);
            exit;
        }
    }
    header("HTTP/1.0 404 Not Found");
}
else
{
    header("HTTP/1.0 403 Forbidden");
}

?>
