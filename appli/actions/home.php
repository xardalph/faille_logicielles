<?php
include("fragments/head.php");


if($user)
{
    // A user is connected.

?>
<div id="connect">
    <p>Connected as <?php echo $user["login" ];?>.</p>
    <p><a href="?action=disconnect">Disconnect</a></p>
</div>




<?php
if($user["isadmin"]){

?>
<div id="admin">
<p>Administration:</p>
<ul>
<?php
    $users = getUserList($_POST["filter"]);
    if($users)
    {
        foreach($users as $u)
        {
            echo "<li>$u <a href=\"?action=remuser&id=$u\">(X)</a></li>\n";
        }
    }
    else
    {
        echo "<li>No user</li>\n";
    }
?>
<form action="?" method="post">
    <input type="text" name="filter" />
    <input type="submit" value="Filter" />
</form>
</ul>
</div>
<?php
} // if(is admin)
?>




<?php
    if($filename)
    {
?>
<div id="fileview">
<div id="path"><?php echo $currentFile ?>:</div>
<div id="content">
<?php
        if(isset($path) && isset($filename) && isset($type) && isset($currentFile))
        {
            if($type=="text")
                include_once($filename);
            else if($type=="image")
                echo "<img src=\"?action=view&file=$currentFile\" />\n";
            else
                echo "<p>other</p>\n";
        }
        else
        {
            print_r($filename);
            print_r($type);
        }
?>
</div>
</div>
<?php
}
?>


<div id="filelist">
<?php
    $content = enumerateFiles($user["login"]);
    if(!$content || count($content)==0)
        echo "<p>Folder empty.</p>\n";
    else
    {
        echo "<ul>\n";
        foreach ($content as $file)
        {
            $url    = "rep/".$user['login'].'/'.$file;
            $delurl = "?action=delete&file=".$file;
            echo "<li><a href=\"$url\">$file</a> <a href=\"$delurl\">(X)</a></li>\n";
        }
        echo "</ul>\n";
        echo "<p><a href=\"?action=deleteall\">Delete all</a></p>\n";
    }
?>
        <form method="post" action="?action=post" enctype="multipart/form-data">
            <input type="file" name="sentfile" /><br />
            <input type="submit" value="Upload file !" />
        </form>
</div>

<?php


}
else
{
    // No user connected.


?>
<div id=connect>
<p>Please, connect yourself:</p>
<form action="?" method="POST"/>
    <input type="hidden" name="action" value="connect" />
    <table border="0">
        <tr><th style="align:right">login :</th><td><input type="text" name="login" /></td></tr>
        <tr><th style="align:right">password :</th><td><input type="password" name="passwd" /></td></tr>
        <tr><td /><td style="align:right" rowspan="2"><input type="submit" value="Connect !"/></td></tr>
    </table>
</form>
<p>Or create an account</p>
<form action="?" method="POST"/>
    <input type="hidden" name="action" value="create" />
    <table border="0">
        <tr><th style="align:right">login :</th><td><input type="text" name="login" /></td></tr>
        <tr><th style="align:right">password :</th><td><input type="password" name="passwd" /></td></tr>


        <tr><th style="align:right"></th><td><img id="captcha" src="/tp1/securimage/securimage_show.php" alt="CAPTCHA Image" /></td></tr>
        <tr><th style="align:right">entrez le texte de l'image</th><td><input type="text" name="captcha_code" size="10" maxlength="6" /></td></tr>
        <tr><th style="align:right"></th><td><a href="#" onclick="document.getElementById('captcha').src = '/tp1/securimage/securimage_show.php?' + Math.random(); return false">[ Different Image ]</a></td></tr>

        <tr><td /><td style="align:right" rowspan="2"><input type="submit" value="Create !"/></td></tr>
    </table>
</form>
</div>
<?php

}
include("fragments/foot.php");

?>

