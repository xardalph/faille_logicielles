<?php

if($user)
{
    if($user["isadmin"])
    {
        $u = $_GET["id"];
        if($u)
            removeUser($u);
        unset($u);
    }
    else
    {
        $msg[] = "You must be administrator to delete an account.";
    }
}
else
{
    $msg[] = "You must be connected to delete files.";
}

$action = "home";

?>
