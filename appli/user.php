<?php

// Declare some usefull functions:

function identifyUser($login){
    global $user, $conf;
    $cookie = substr(md5(rand()), 0, 30);
    setUserCookie($login, $cookie);

    $user["login"] = $login;
    $user["cookie"]= $cookie;
    $user["isadmin"]= userIsAdmin($login);

    setcookie("cookie",  $user["cookie"]);
    setcookie("isadmin", $user["isadmin"]);

    return $user;
}

function forgetUser(){
    global $user, $conf;
    include_once("db.php");
    deletecookiebd($user["login"]);
    $user = null;
    setcookie("cookie", null);
    setcookie("isadmin", null);
}

function getUserRepositoryPath($login){
    global $conf;
    return $conf["repository"]."/".$login;
}

function enumerateFiles($login){
    global $conf;

    $rep = getUserRepositoryPath($login);

    // Ensure dir exists
    if(!file_exists($rep))
    {
        mkdir($rep) or die("Can't create directory : ".$rep);
        chmod($rep, 0777);
    }

    $content = null;

    if($dir = opendir($rep)) {
        while (false !== ($file = readdir($dir))) {
            if($file[0]!='.')
                $content[] = $file;
        }
    }
    return $content;
}

function recognizeUser(){
    global $user, $conf;

    $cookie = $_COOKIE["cookie"];
    if($cookie)
    {
        $login = getNameFromCookie($cookie);
        if($login)
        {
            $user["login"] = $login;
            $user["cookie"]= $cookie;
            $user["isadmin"]= userIsAdmin($login);
            return $user;
        }
    }
    forgetUser();
    return null;
}

function deleteAllFiles($login){
    global $conf;

    $files = enumerateFiles($login);
    $path = getUserRepositoryPath($login);
    if($files)
    {
        foreach($files as $f)
        {
            $filename = $path."/".$f;
            unlink($filename);
        }
    }
}

function removeUser($login){
    deleteAllFiles($login);
    rmdir(getUserRepositoryPath($login));
    removeUserEntry($login);
}

?>
