/**
 * Intend to create a predefined account on the folder web application.
 * 
 * @author Emilien Kia
 */
package net.emilienkia.messi.tp1;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Execute:
 *
 * > mvn install exec:java
 */
public class Main {

    /** Base address of folder application to connect to. */
    static final String addr = "http://localhost/tp1/appli/";
    /** Number of account to create. */
    static final int number = 5;
    /** Default password to use for created account. */
    static final String passwd = "plop";

    public static void main(String[] args) {

        try {

            for (int i = 0; i < number; i++) {
                URL url = new URL(addr + "?action=create&login=qqun" + i + "&passwd=" + passwd);
                HttpURLConnection cnx = (HttpURLConnection) url.openConnection();
                cnx.connect();
                System.out.println("qqun" + i + " => " + cnx.getResponseCode());
            }

            System.out.println("Done");

        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
    }
}
