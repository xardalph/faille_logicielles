<?php

function printDir($rep){
    echo "<p>$rep :</p><ul>\n";
    if($dir = opendir($rep)) {
        while (false !== ($file = readdir($dir))) {
            $content = htmlentities(file_get_contents($file));
            echo "<li>$file<pre>$content</pre></li>\n";
        }
    }
    echo "</ul>\n";
}

printDir(".");
?>
